import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; //important component allows React Bootstrap Works
import { styleRelativeToWidth } from './jsScripts/styleScripts';
import LeftBlackTabBar from './Components/LeftBlackTabBar';

//import Dashboard from './Components/Dashboard/Dashboard';
//import Loads from './Components/Loads';
//import Loadboard from './Components/Loadboard';
//import Offers from './Components/Offers';
//import Tracking from './Components/Tracking';
//import Loads from './Components/Loads';
import Contacts from './Components/Contacts'

export default class App extends Component {
  state = {
    fullWindowDivStyle: fullWindowDivStyle(),
    componentToMount:<Contacts/> //the first Component which we render in the start screen
  }
  componentDidMount(){
    window.addEventListener('resize',() => this.setState({fullWindowDivStyle: fullWindowDivStyle()}));
    window.AppComponent = this //very Importan feature!!! Make access to App component from other external components
  }

  render(){
    return (
      <div style = {this.state.fullWindowDivStyle}> 
        <LeftBlackTabBar/>
          {this.state.componentToMount}
      </div>

    );
  };
};

function fullWindowDivStyle(){
  //This is the style of the main App window

  return {
    display:"flex", 
    flexDirection: styleRelativeToWidth("column","column","row","row","row"),
    backgroundColor: 'gray',//'#f4f5f9', 
    height:'100vh', 
    width:'100%', 
    left:0, 
    top:0, 
  };
};

