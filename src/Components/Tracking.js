import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';


export default class Dashboard extends Component {
    state = {
        styles:thisComponentStyles(),
    };

    static defaultProps = {
        center: {
          lat: 40.73,
          lng: -73.93
        },
        zoom: 9
      };

    render(){
        return(
            <div style = {this.state.styles.mainDivStyle}>
                <div style={{ height: '100vh', width: '100%' }}>
                        <GoogleMapReact
                        bootstrapURLKeys={{ key:"AIzaSyCmxjMbIRZjJ8j6WPWMVDy4qJLcYKaznGo"}}
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}
                        >
                        </GoogleMapReact>
                </div>


            </div>
        );
    };
};

function thisComponentStyles(){
    return {
        mainDivStyle:{
            backgroundColor:'#f4f5f9',
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'flex-start',
            flexDirection:'column'
        },

    };
};