import React, {Component} from 'react';
import { Tab, Divider, } from '@material-ui/core';
import {TabPanel, TabContext, TabList} from '@material-ui/lab'


export default class Loadboard extends Component {
    state = {
        styles:thisComponentStyles(),
        tabContextValue:'Availble Loads Value'
    };

    superLoadBoardTabsValue (labelName){ //function that represents the name of tab with all properties
        return (
            <div style={{display:'flex', justifyContent:'space-between',backgroundColor:'white',width:'100%'}}>
                <p>{labelName}</p>
                <p>0</p>
            </div>
        )
    }

    render(){
        return(
            <div style = {this.state.styles.mainDivStyle} >
                <div style={{display:'flex', height:'5vh',width:'100%',backgroundColor:'#2082fa'}}></div>

                <TabContext value={this.state.tabContextValue} >
                    <div style={{display:'flex', flexDirection:'row'}}>
                        <TabList onChange={(event,value)=>this.setState({tabContextValue:value})} orientation="vertical" indicatorColor='primary'
                        style={{width:250,height:'95vh',backgroundColor:'white',justifyContent:'center'}}>
                            <h4 style={{textAlign:'center',marginTop:20,marginBottom:20}}>Super Loadboard</h4>
                            <Tab label={this.superLoadBoardTabsValue("Availble Loads")} value="Availble Loads Value" />
                            <Tab label={this.superLoadBoardTabsValue("Requested")} value="Requested Value" />
                            <Tab label={this.superLoadBoardTabsValue("Booked")} value="Booked Value" />
                            <Tab label={this.superLoadBoardTabsValue("Suggested")} value="Suggested Value" />
                            <Divider variant='middle'/>
                        </TabList>
                        <Divider orientation='vertical'/>
                        <div style={{width:'100%'}}>
                            <div style={{backgroundColor:'white',width:'100%',height:75}}></div>
                            <Divider/>
                            <TabPanel value="Availble Loads Value" >New Loads</TabPanel>
                            <TabPanel value="Requested Value">Assigned</TabPanel>
                            <TabPanel value="Booked Value">Picked</TabPanel>
                            <TabPanel value="Suggested Value">Delivered</TabPanel>
                        </div>
                    </div>    
                </TabContext>
            </div>
        );
    };
};

function thisComponentStyles(){

    return {
        mainDivStyle:{
            backgroundColor:'#f4f5f9',
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'flex-start',
            flexDirection:'column'},

    };
};