import React, {Component} from 'react';
import { styleRelativeToWidth } from '../jsScripts/styleScripts';
import { Icon } from 'react-icons-kit';
import {stumbleupon2} from 'react-icons-kit/icomoon/stumbleupon2'
import {meter} from 'react-icons-kit/icomoon/meter'
import {subway} from 'react-icons-kit/fa/subway'
import {weibo} from 'react-icons-kit/entypo/weibo'
import {mobileCombo} from 'react-icons-kit/entypo/mobileCombo'
import {exportIcon} from 'react-icons-kit/entypo/exportIcon'
import {speedometer} from 'react-icons-kit/ionicons/speedometer'
import {locationArrow} from 'react-icons-kit/typicons/locationArrow'
import {u1F69B} from 'react-icons-kit/noto_emoji_regular/u1F69B'

import Offers from './Offers';
import Loadboard from './Loadboard';
import Dashboard from './Dashboard/Dashboard';
import Loads from './Loads';
import Tracking from './Tracking';
import Contacts from './Contacts';
import Drivers from './Drivers';


//-----------------------------------------------------------------------------------------------------------------------------------------------------

export default class LeftBlackTabBar extends Component {
    state = {
        styles: leftBlackTabBarStyles(),
    }; 
    
    componentDidMount(){
        window.addEventListener('resize',() => this.setState({styles: leftBlackTabBarStyles()}));
    };

    render(){
        return(
        <div style = {this.state.styles.leftBlackTabBarDiv}>
            
            <div style = {{color: '#f4f5f9', marginTop:20}}><Icon size = {32} icon = {stumbleupon2}/></div>

            <div style = {this.state.styles.separator}/>

            <OneButton name = {'LoadBoard'} icon={subway} componentToMount = {<Loadboard/>}/>

            <OneButton name = {'Offers'} icon={exportIcon} componentToMount = {<Offers/>}/>
                                               
            <div style = {this.state.styles.separator}/>

            <OneButton name = {'DashBoard'} icon={meter} componentToMount = {<Dashboard/>}/>

            <OneButton name = {'Loads'} icon={u1F69B} componentToMount = {<Loads/>}/>

            <OneButton name = {'Trips'} icon={speedometer} />

            <OneButton name = {'Tracking'} icon={locationArrow} componentToMount = {<Tracking/>}/>
         
            <div style = {this.state.styles.separator}/>
            
            <OneButton name = {'Contacts'} icon={mobileCombo} componentToMount={<Contacts/>}/>

            <OneButton name = {'Drivers'} icon={weibo} componentToMount={<Drivers/>}/>

            <div style = {this.state.styles.separator}/>
            
            <div style = {{color: '#f4f5f9', marginBottom:20}}><Icon size = {32} icon = {stumbleupon2}/></div>
        </div>
        );
    };
};


class OneButton extends Component {
    state = {
        styles: leftBlackTabBarStyles(),
        buttonIsActiveColor: {backgroundColor:'#1b2232'},
        buttonIsActive:false   
    }; 

    onMouseLeaveBackground = () => {
        if(!this.state.buttonIsActive){      //logic to understand is button active or not. And changes background or not
            this.setState({buttonIsActiveColor: {backgroundColor: '#1b2232'}}); //if not active backgound changes to its usual value, when mouse leaves it
        };
    };

    onMouseEnterHandle = () => {
        this.setState({buttonIsActiveColor: {backgroundColor: '#343b4d'}});
    };

    onClickHandle = () => {
            window.AppComponent.setState({componentToMount:this.props.componentToMount});   //Which component we want to mount in the App.js

            window.LeftBlackTabBarButtonIsActive.setState({buttonIsActiveColor: {backgroundColor: '#1b2232'}, buttonIsActive:false})    //changes the state and background of previous button to inactive
            window.LeftBlackTabBarButtonIsActive = this;    //assign new button to window component
            this.setState({buttonIsActiveColor: {backgroundColor: '#343b4d'},buttonIsActive:true}); //changes state and background to active in new clicked button
    };

    componentDidMount(){
        window.addEventListener('resize',() => this.setState({styles: leftBlackTabBarStyles()}));   //changes background style in case of window resize

        window.LeftBlackTabBarButtonIsActive = this //this is the first state (like memory in window component)for button activation. Do not delete. In another case, there will be an error

    };

    render(){
        return (
            <button style = {Object.assign({},this.state.buttonIsActiveColor,this.state.styles.button) } //Object.assign copies properties to first empty object from another not empty objects
                    onClick = {this.onClickHandle}
                    onMouseEnter = {this.onMouseEnterHandle}
                    onMouseLeave = {this.onMouseLeaveBackground}
                >
                    <div style={{color: '#f4f5f9', marginTop:0, marginBottom:5}}><Icon size={20} icon={this.props.icon}/></div>
                    <p style={{fontSize:13}}> {this.props.name} </p>
            </button>
        );
    };
};

//------------------------------------------------------------------------------------------------------------------------------------------------------------
//Styles

function leftBlackTabBarStyles(){
    //width of Left BlacK Tab Bar menu 
    const leftBlackTabBarWidth = styleRelativeToWidth(50,75,80,80,80);
        
    return {
      leftBlackTabBarDiv:{
          backgroundColor: '#1b2232', 
          width: styleRelativeToWidth(window.innerWidth,window.innerWidth,leftBlackTabBarWidth,leftBlackTabBarWidth,leftBlackTabBarWidth),
          height: styleRelativeToWidth(leftBlackTabBarWidth,leftBlackTabBarWidth,window.innerHeight, window.innerHeight, window.innerHeight),
          display:'flex',
          flexDirection: styleRelativeToWidth("row","row","column","column","column"),
          justifyContent:'space-around',
          alignItems: 'center',
        },

      button:{
        width: styleRelativeToWidth(leftBlackTabBarWidth*1.2 ,leftBlackTabBarWidth*1.2,leftBlackTabBarWidth*0.9,leftBlackTabBarWidth*0.9,leftBlackTabBarWidth*0.9),
        height: styleRelativeToWidth(leftBlackTabBarWidth,leftBlackTabBarWidth,leftBlackTabBarWidth/1.3,leftBlackTabBarWidth/1.3,leftBlackTabBarWidth/1.3),
        //backgroundColor:'#1b2232', //'#343b4d',
        border:'none',
        color:'white',
      },

      separator:{
          width:100,
          height:30,
          //backgroundColor:"white"
      },

      leftBlackTabBarWidth:leftBlackTabBarWidth // exports to extrnal component!!! 

  }};
  
  