import React, {Component} from 'react';

export default class Offers extends Component {
    state = {
        styles:thisComponentStyles(),
    };

    render(){
        return(
        <div style = {this.state.styles.mainDivStyle}>
        </div>
        );
    };
};

function thisComponentStyles(){
    return {
        mainDivStyle:{
            backgroundColor:'#f4f5f9',
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'flex-start',
            flexDirection:'column'},
    };
};