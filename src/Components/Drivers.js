import React, {Component} from 'react';

export default class Dashboard extends Component {
    state = {
        styles:thisComponentStyles(),
    };

    render(){
        return(
            <div style = {this.state.styles.mainDivStyle}>
                <div style = {{backgroundColor:'white',}}>
                    <h3 style={{margin:20}}>Contacts</h3>
                </div>
                <div style = {this.state.styles.contentField}>

                </div>
            </div>
        );
    };
};

function thisComponentStyles(){
    return {
        mainDivStyle:{
            backgroundColor:'#f4f5f9',
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'flex-start',
            flexDirection:'column'
        },

        contentField:{
            display:'flex',
            flexWrap:'wrap',
            justifyContent:'flex-start'

        },

        content:{
            width:'30%',
            height:150, 
            backgroundColor:'white', 
            margin:20,
            borderRadius:5
            
        },

        contentName:{
            margin:15,
        },

        contentProperty:{
            margin:15,
            marginTop:40,
        },
    };
};