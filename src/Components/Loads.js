import React, {Component} from 'react';
import { Tabs,Tab,Button, DropdownButton, Dropdown, InputGroup, FormControl } from 'react-bootstrap';

export default class Loads extends Component {
    state = {
        styles:thisComponentStyles(),
    };

    render(){
        return(
            <div style = {this.state.styles.mainDivStyle}>
                <LoadsTopControlPanel/>
                <LoadsTabs/>
            </div>
        );
    };
};

class LoadsTopControlPanel extends Component {
    state = {
        style:thisComponentStyles(),
    }

    render(){
        return(
        <div style={this.state.style.loadTopControlPanelStyle}>

            <Button variant='primary' size='sm'>Create Load</Button>

            <Button variant='outline-secondary'style={{marginLeft:20}} size='sm'>Import Dispatch sheet</Button>

            <DropdownButton title="All Drivers" style={{marginLeft:60}} size='sm' variant='outline-primary'>
                <Dropdown.Item >All Drivers</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >First</Dropdown.Item>
                <Dropdown.Item >Second</Dropdown.Item>
                <Dropdown.Item >Third</Dropdown.Item>
            </DropdownButton>

            <DropdownButton title="All Dispetchers" style={{marginLeft:60}} size='sm' variant='outline-primary'>
                <Dropdown.Item >All Dispetchers</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >First</Dropdown.Item>
                <Dropdown.Item >Second</Dropdown.Item>
                <Dropdown.Item >Third</Dropdown.Item>
            </DropdownButton>

            <DropdownButton title="Sort by:" style={{marginLeft:60}} size='sm' variant='outline-primary'>
                <Dropdown.Item >Reset</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Customer</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Pickup City</Dropdown.Item>
                <Dropdown.Item >Deliver City</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Pickup State</Dropdown.Item>
                <Dropdown.Item >Deliver State</Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item >Pickup Date</Dropdown.Item>
                <Dropdown.Item >Deliver Date</Dropdown.Item>
            </DropdownButton>

            <DropdownButton title="Load ID" style={{marginLeft:80}} size='sm' variant='outline-primary'>
                <Dropdown.Item >Load ID</Dropdown.Item>
                <Dropdown.Item >Order ID</Dropdown.Item>
                <Dropdown.Item >Vin</Dropdown.Item>
                <Dropdown.Item >Make, Model, Year</Dropdown.Item>
                <Dropdown.Item >Pickup Name</Dropdown.Item>
                <Dropdown.Item >Delivery Name</Dropdown.Item>
                <Dropdown.Item >Pickup Date</Dropdown.Item>
                <Dropdown.Item >Deliver Date</Dropdown.Item>
                <Dropdown.Item >Internal Load ID</Dropdown.Item>
                <Dropdown.Item >Invoice ID</Dropdown.Item>
                <Dropdown.Item >Broker Name</Dropdown.Item>
                <Dropdown.Item >All</Dropdown.Item>

            </DropdownButton>

            <InputGroup style={{width:200,marginLeft:60}}>
                <FormControl size='sm' placeholder='Search for orders'
                />
                <InputGroup.Append>
                    <Button size='sm' variant='outline-primary'>
                        Find
                    </Button>
                </InputGroup.Append>

            </InputGroup>

        </div>

        )
    } 
};

class LoadsTabs extends Component {
    state = {
        tabContextValue:"New Loads Value",
    }

    render(){
        return(
            <Tabs defaultActiveKey='new-loads-key' style={{marginLeft:20}}>
                <Tab eventKey="new-loads-key" title="New Loads">
                    <h1>New Loads</h1>
                </Tab>
                <Tab eventKey="assigned-key" title="Assigned">
                    <h1>assigned</h1>
                </Tab>
                <Tab eventKey="picked-up-key" title="Picked Up">
                </Tab>
                <Tab eventKey="delivered-key" title="Delivered">
                </Tab>
                <Tab eventKey="billed-key" title="Billed">
                </Tab>
                <Tab eventKey="paid-key" title="Paid">
                </Tab>
                <Tab eventKey="archived-key" title="Archived">
                </Tab>
                <Tab eventKey="deleted-key" title="Deleted">
                </Tab>


            </Tabs>


        )
    }
}

function thisComponentStyles(){
    return {
        mainDivStyle:{
            backgroundColor:'#f4f5f9',
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'flex-start',
            flexDirection:'column'},
        loadTopControlPanelStyle:{
            display:'flex',
            flexDirection:'row',
            //backgroundColor:'lightgray',
            margin:20,

        },
    };

};