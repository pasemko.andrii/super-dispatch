export const styleRelativeToWidth = function (extraSmallDisp, smallDisp, mediumDisp, largeDisp, extraLargeDisp){
  
    //function that returns the value of style, depending on the screen size (like Bootstrap grid system)

    if (window.innerWidth < 576){
        return extraSmallDisp;
    }
    else if(window.innerWidth >= 576 && window.innerWidth < 768){
        return smallDisp;
    }
    else if(window.innerWidth >= 768 && window.innerWidth < 992){
        return mediumDisp;
    }
    else if (window.innerWidth >= 992 && window.innerWidth <= 1366){
        return largeDisp;
    }
    else{
        return extraLargeDisp;
    };
};
